/*
 * bike_computer.h
 *
 *  Created on: Jan 12, 2020
 *      Author: fri
 */

#ifndef INC_BIKE_COMPUTER_H_
#define INC_BIKE_COMPUTER_H_

void led_task(void* argument);
void pulse_counter_task(void* argument);
void display_task(void* argument);
void trip_timer();
void button_reader_task(void* argument);

void bike_computer_init_values();


#endif /* INC_BIKE_COMPUTER_H_ */
