/*
 * bike_computer.c
 *
 *  Created on: Jan 12, 2020
 *      Author: fri
 */

#include "bike_computer.h"
#include "ssd1306.h"
#include "ssd1306_tests.h"
#include "cmsis_os.h"
#include "stdint.h"
#include "math.h"
#include "stdio.h"
#include "main.h"
#include "queue.h"
#include "semphr.h"
#include "string.h"


QueueHandle_t input_queue;
volatile SemaphoreHandle_t timer_semaphore;
volatile struct
{
	uint32_t current_pulses;
	uint32_t total_pulses;

	uint32_t current_time_s;

	uint32_t current_absolute_time_s;

	uint32_t speed_kph;
	uint32_t time_diff_ms;

	uint16_t adc_value;



	uint8_t green_flash;

	uint8_t red_flash;
}shared_data;
enum main_elements
{
	ELEMENT_AVG_SPD,
	ELEMENT_CUR_DST,
	ELEMENT_CUR_T,
	ELEMENT_TOT_D,
	NUM_ELEMENTS
};
struct
{
	float avg_speed;
	int ontime_speed;
	uint32_t ontime_dist_km;
	uint32_t ontime_time_m;
	uint8_t ontime;
	int dark_theme;
	enum main_elements upper_main;
	enum main_elements lower_main;
}data;


enum button_name
{
	BUTTON_LEFT,
	BUTTON_RIGHT,
	BUTTON_UP,
	BUTTON_DOWN,
	NUM_OF_BUTTONS
};
enum button_state
{
	BUTTON_PRESS,
	BUTTON_HOLD
};
enum register_map
{
	RTC_REGISTER_TOTAL_PULSES=1,
	RTC_REGISTER_CURRENT_PULSES,
	RTC_REGISTER_CURRENT_TIME,
	RTC_REGISTER_WHEEL_SIZE,
	RTC_REGISTER_LAST_TIME,
	RTC_REGISTER_LAST_DIST,
	RTC_REGISTER_TEST,
	RTC_REGISTER_LOWER_MAIN,
	RTC_REGISTER_HIGHER_MAIN,
	RTC_REGISTER_DARK_THEME,
};
enum menu_options
{
	OPTION_ONTIME = 0,
	OPTION_DARKMODE,
	OPTION_SETWHEEL,
	OPTION_DEBUG,
	OPTION_RESET_TRIP,
	NUM_MENU_OPTIONS
};

const char * menu_option_text[] =
{
		"OnTime",
		"Theme",
		"Wheel",
		"Debug",
		"RSTtrip",
};
struct
{
	GPIO_TypeDef *port;
	uint16_t pin;
	GPIO_PinState previous_state;
}button_descriptions[NUM_OF_BUTTONS];

struct button_event
{
	enum button_name name;
	enum button_state state;
};
struct
{

	uint32_t diameter_cm;
	uint16_t speed_timeout_ms;

}bike_parameters;
struct button_event main_button_event;
RTC_HandleTypeDef *main_hrtc;

char print_buffer[32];

void debug_view();
void set_ontime_view();
void reset_trip_view();
void main_view();
void calc_ontime();
void set_wheel_view();
uint8_t number_input(int32_t *number, char *title,int32_t start, int32_t upper, int32_t lower, uint8_t jump);

void write_RTC(uint32_t address, uint32_t value)
{
	HAL_RTCEx_BKUPWrite(main_hrtc, address, value);
}
uint32_t read_RTC(uint32_t address)
{
	return HAL_RTCEx_BKUPRead(main_hrtc, address);
}

void bike_computer_init_values(RTC_HandleTypeDef *rtc)
{
	button_descriptions[BUTTON_LEFT].port = LEFT_BUTTON_GPIO_Port;
	button_descriptions[BUTTON_LEFT].pin = LEFT_BUTTON_Pin;

	button_descriptions[BUTTON_RIGHT].port = RIGHT_BUTTON_GPIO_Port;
	button_descriptions[BUTTON_RIGHT].pin = RIGHT_BUTTON_Pin;

	button_descriptions[BUTTON_UP].port = UP_BUTTON_GPIO_Port;
	button_descriptions[BUTTON_UP].pin = UP_BUTTON_Pin;

	button_descriptions[BUTTON_DOWN].port = DOWN_BUTTON_GPIO_Port;
	button_descriptions[BUTTON_DOWN].pin = DOWN_BUTTON_Pin;

	timer_semaphore = xSemaphoreCreateMutex();

	main_hrtc = rtc;

	input_queue=xQueueCreate(10,sizeof(main_button_event));

	bike_parameters.diameter_cm = read_RTC(RTC_REGISTER_WHEEL_SIZE);
	if(bike_parameters.diameter_cm == 0)
	{
		bike_parameters.diameter_cm = 60;
	}
	bike_parameters.speed_timeout_ms = 2000;


	shared_data.current_pulses = read_RTC(RTC_REGISTER_CURRENT_PULSES);
	shared_data.total_pulses = read_RTC(RTC_REGISTER_TOTAL_PULSES);
	shared_data.current_time_s = read_RTC(RTC_REGISTER_CURRENT_TIME);
	shared_data.adc_value = 0;
	shared_data.green_flash = 0;
	shared_data.red_flash = 0;

	data.ontime = 0;
	data.lower_main = read_RTC(RTC_REGISTER_LOWER_MAIN);
	data.upper_main = read_RTC(RTC_REGISTER_HIGHER_MAIN);

	data.dark_theme = read_RTC(RTC_REGISTER_DARK_THEME);
	ssd1306_DarkTheme(data.dark_theme);
}
void led_task(void* argument)
{
	while(1)
	{

		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
		if(shared_data.red_flash)
		{
			HAL_GPIO_WritePin(GPIO_RED_LED_GPIO_Port, GPIO_RED_LED_Pin, GPIO_PIN_SET);
		}
		if(shared_data.green_flash)
		{
			HAL_GPIO_WritePin(GPIO_GREEN_LED_GPIO_Port, GPIO_GREEN_LED_Pin, GPIO_PIN_SET);
		}

		osDelay(500);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIO_RED_LED_GPIO_Port, GPIO_RED_LED_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIO_GREEN_LED_GPIO_Port, GPIO_GREEN_LED_Pin, GPIO_PIN_RESET);

		//SAVE PULSES AND TIME
		write_RTC(RTC_REGISTER_TOTAL_PULSES,shared_data.total_pulses);
		write_RTC(RTC_REGISTER_CURRENT_PULSES, shared_data.current_pulses);
		write_RTC(RTC_REGISTER_CURRENT_TIME, shared_data.current_time_s);

		osDelay(500);

		if(shared_data.speed_kph > 0)
		{
			shared_data.current_time_s++;
		}
		shared_data.current_absolute_time_s++;
	}
}
void trip_timer()
{
	shared_data.current_time_s++;
}
uint16_t read_adc(ADC_HandleTypeDef *adc)
{
	HAL_ADC_PollForConversion(adc, HAL_MAX_DELAY);
	uint16_t level = HAL_ADC_GetValue(adc);
	shared_data.adc_value = level;
	return level;
}
void button_reader_task(void *argument)
{
	while(1)
	{
		osDelay(5);
		static uint16_t hold_timers[NUM_OF_BUTTONS];
		for(uint8_t i = 0; i<NUM_OF_BUTTONS; i++)
		{
			GPIO_PinState current_state = HAL_GPIO_ReadPin(button_descriptions[i].port, button_descriptions[i].pin);
			if(current_state == GPIO_PIN_SET)
			{
				hold_timers[i] = 0;
			}
			else
			{
				hold_timers[i]++;
			}
			if(hold_timers[i] > 50)
			{
				hold_timers[i] = 0;
				//button held, generate event
				struct button_event event;
				event.name = i;
				event.state = BUTTON_HOLD;
				xQueueSend(input_queue, &event, 0);

			}
			if(button_descriptions[i].previous_state == GPIO_PIN_SET && current_state == GPIO_PIN_RESET)
			{
				//button was pressed. Generate event
				struct button_event event;
				event.name = i;
				event.state = BUTTON_PRESS;
				xQueueSend(input_queue, &event, 0);
			}

			button_descriptions[i].previous_state = current_state;
		}
	}



}
void calculate_speed(uint32_t pulse_diff_time_ms)
{
	shared_data.time_diff_ms = pulse_diff_time_ms;
	float speed_mps;
	float speed_kph;
	if(pulse_diff_time_ms > bike_parameters.speed_timeout_ms)
	{
		speed_mps=0;
	}
	else
	{
		double rotations_per_s = pow(pulse_diff_time_ms,-1)*1000;
		uint32_t rotations_per_minute = rotations_per_s *60;
		speed_mps = rotations_per_minute*(2*3.14)/60 * (bike_parameters.diameter_cm/2/100.0);
	}

	speed_kph = speed_mps * 3.6;
	shared_data.speed_kph= speed_kph;
}
void pulse_counter_task(void* argument)
{
	ADC_HandleTypeDef *adc;
	uint16_t upper_level = 1960;
	uint16_t lower_level = 1885;
	uint16_t histeresis = 20;
	uint32_t prev_pulse_time = osKernelGetTickCount();
	adc=argument;
	while(1)
	{
		osDelay(1);
		uint16_t level = read_adc(adc);
		uint32_t cur_pulse_time = osKernelGetTickCount();
		if(cur_pulse_time-prev_pulse_time >= bike_parameters.speed_timeout_ms)
		{
			calculate_speed(bike_parameters.speed_timeout_ms+1);
		}
		if(level>upper_level || level<lower_level)
		{
			calculate_speed(cur_pulse_time-prev_pulse_time);
			prev_pulse_time = cur_pulse_time;
			osDelay(80); //prevent bouncing
			shared_data.current_pulses++;
			shared_data.total_pulses++;
			while(level>upper_level-histeresis || level<lower_level+histeresis)
			{
				level = read_adc(adc);
				osDelay(1);
			}
		}
	}
}

uint8_t recv_button_event(struct button_event *event)
{
	static struct button_event event_recv;
	if(xQueueReceive(input_queue, &event_recv, 0) == pdPASS)
	{
		*event=event_recv;
		return 1;
	}
	return 0;
}
void draw_current_time(uint8_t x, uint8_t y)
{
	uint16_t minutes;
	uint16_t seconds;
	uint32_t time = shared_data.current_time_s;

	minutes = time/60;
	seconds = time%60;
	ssd1306_SetCursor(x, y+5);
	sprintf(print_buffer," cTime ");
	ssd1306_WriteString(print_buffer, Font_7x10, White);

	ssd1306_SetCursor(x+44, y);
	sprintf(print_buffer,"%3d:%02d ",minutes,seconds);
	ssd1306_WriteString(print_buffer, Font_11x18, White);

}
void draw_current_dist(uint8_t x, uint8_t y)
{
	uint8_t decimal;
	uint16_t km;

	uint32_t pulses = shared_data.current_pulses;

	float distance_km = 3.14 * (bike_parameters.diameter_cm/100.0) * pulses;
	distance_km /= 1000;

	km = roundf(distance_km);

	decimal = ((int)(distance_km*100)%100);


	ssd1306_SetCursor(x, y+5);

	sprintf(print_buffer," cDist ");
	ssd1306_WriteString(print_buffer, Font_7x10, White);

	ssd1306_SetCursor(x+44, y);
	sprintf(print_buffer,"%3d.%02d ",km,decimal);
	ssd1306_WriteString(print_buffer, Font_11x18, White);
}
void draw_tot_dist(uint8_t x, uint8_t y)
{
	uint8_t decimal;
	uint16_t km;

	uint32_t pulses = shared_data.total_pulses;

	float distance_km = 3.14 * (bike_parameters.diameter_cm/100.0) * pulses;
	distance_km /= 1000;

	km = roundf(distance_km);

	decimal = ((int)(distance_km*100)%100);


	ssd1306_SetCursor(x, y+5);

	sprintf(print_buffer," tDist ");
	ssd1306_WriteString(print_buffer, Font_7x10, White);

	ssd1306_SetCursor(x+44, y);
	sprintf(print_buffer,"%3d.%02d ",km,decimal);
	ssd1306_WriteString(print_buffer, Font_11x18, White);
}
void calc_avg_speed()
{


	uint32_t pulses = shared_data.current_pulses;

	float distance_m = 3.14 * (bike_parameters.diameter_cm/100.0) * pulses;

	uint32_t time = shared_data.current_time_s;
	float avg_spd;
	if(time > 0)
	{
		avg_spd = distance_m / (time);
		avg_spd *= 3.6;
	}
	else
	{
		avg_spd = 0;
	}
	data.avg_speed = avg_spd;
}
void draw_avg_speed(uint8_t x, uint8_t y)
{
	uint8_t decimal;
	uint16_t kph;


	kph = roundf(data.avg_speed);

	decimal = ((int)(data.avg_speed*10)%10);

	ssd1306_SetCursor(x, y+5);

	sprintf(print_buffer," aSpd ");
	ssd1306_WriteString(print_buffer, Font_7x10, White);

	ssd1306_SetCursor(x+44, y);
	sprintf(print_buffer,"%3d.%1d ",kph,decimal);
	ssd1306_WriteString(print_buffer, Font_11x18, White);
}
void main_view()
{
	uint8_t refresh_counter = 0;
	int8_t selected_element = 0;
	uint8_t selection = 0;
	uint8_t dist = 0;
	int8_t dist_dir = 1;
	ssd1306_Fill(Black);
	while(1)
	{
		struct button_event event;
		if(recv_button_event(&event))
		{
			refresh_counter = 0;
			if(event.name == BUTTON_DOWN && event.state == BUTTON_PRESS)
			{
				shared_data.red_flash = 0;
				shared_data.green_flash = 0;
				ssd1306_Fill(Black);
				return;
			}
			if(event.name == BUTTON_UP && event.state == BUTTON_PRESS)
			{
				selection = 20;
				selected_element++;
				dist = 0;
				if(selected_element > 1)
				{
					selected_element = 0;
				}

			}
			enum main_elements vm;
			if(selected_element == 0)
			{
				vm = data.lower_main;
			}
			else
			{
				vm = data.upper_main;
			}
			if(event.name == BUTTON_LEFT && event.state == BUTTON_PRESS)
			{
				if(vm == 0)
				{
					vm = NUM_ELEMENTS -1;
				}
				else
				{
					vm--;
				}

			}
			if(event.name == BUTTON_RIGHT && event.state == BUTTON_PRESS)
			{
				vm++;
			}
			if(vm >= NUM_ELEMENTS)
			{
				vm = 0;
			}

			if(selected_element == 0)
			{
				data.lower_main = vm;
				write_RTC(RTC_REGISTER_LOWER_MAIN, data.lower_main);
			}
			else
			{
				data.upper_main = vm;
				write_RTC(RTC_REGISTER_HIGHER_MAIN, data.upper_main);

			}
			ssd1306_Fill(Black);
		}
		if(1)
		{

			ssd1306_SetCursor(0, 0);
			uint32_t speed = shared_data.speed_kph;
			sprintf(print_buffer,"%3d",speed);
			if(refresh_counter == 0)
			{
				ssd1306_WriteString(print_buffer, Font_16x26, White);
				calc_avg_speed();

			}
			//draw_current_time(0, 24);
			//draw_avg_speed(0, 24+20);

			for(uint8_t i = 0; i< 2;i++)
			{
				enum main_elements current_element;
				uint8_t x = 0;
				uint8_t y = 24;
				current_element = data.upper_main;
				if(i==1)
				{
					y=24+20;
					current_element = data.lower_main;
				}
				if(selection > 0 && i != selected_element)
				{
					x+=dist;
					dist+=2*dist_dir;
					if(dist > 10)
					{
						dist_dir = -1;
					}
					if(dist <= 0)
					{
						dist_dir = 1;
					}
					selection--;
				}
				switch (current_element)
				{
				case ELEMENT_AVG_SPD:
					draw_avg_speed(x, y);
					break;
				case ELEMENT_CUR_DST:
					draw_current_dist(x, y);
					break;
				case ELEMENT_CUR_T:
					draw_current_time(x, y);
					break;
				case ELEMENT_TOT_D:
					draw_tot_dist(x, y);
					break;
				}

			}

			if(data.ontime)
			{
				osDelay(5);
				calc_ontime();

				if(speed < data.ontime_speed)
				{
					shared_data.red_flash = 1;
				}
				else
				{
					shared_data.red_flash = 0;
				}
				if(speed > (data.ontime_speed+2) * 1.2)
				{
					shared_data.green_flash = 1;
				}
				else
				{
					shared_data.green_flash = 0;
				}
				ssd1306_SetCursor(70, 0);
				sprintf(print_buffer,"onTime");
				ssd1306_WriteString(print_buffer, Font_7x10, White);
				ssd1306_SetCursor(70, 12);
				sprintf(print_buffer,"%3d",data.ontime_speed);
				ssd1306_WriteString(print_buffer, Font_7x10, White);

				if(!data.ontime)
				{
					shared_data.red_flash = 0;
					shared_data.green_flash = 0;
					ssd1306_Fill(Black);
				}
			}


		}
		refresh_counter++;
		if(refresh_counter > 10)
		{
			refresh_counter = 0;
		}
		ssd1306_UpdateScreen();

		osDelay(50);
	}
}
void draw_arrows(uint8_t center, uint8_t height)
{
	static int8_t dist = 0;
	static int8_t dist_dir = 1;


	ssd1306_SetCursor(center - (strlen(print_buffer)-1)*11/2 - 15 - dist, height);
	ssd1306_WriteChar('<', Font_11x18, White);
	ssd1306_SetCursor(center + (strlen(print_buffer)-1)*11/2 + 15 + dist, height);

	ssd1306_WriteChar('>', Font_11x18, White);

	dist+=dist_dir;
	if(dist>5)
	{
		dist_dir = -1;
	}
	if(dist <= 0)
	{
		dist_dir = 1;
	}
}
void menu_view()
{
	int8_t index = 0;
	while(1)
	{
		struct button_event event;
		if(recv_button_event(&event))
		{
			if(event.name == BUTTON_LEFT)
			{
				index--;
			}
			if(event.name == BUTTON_RIGHT)
			{
				index++;
			}
			if(index < 0)
			{
				index = NUM_MENU_OPTIONS - 1;
			}
			if(index >= NUM_MENU_OPTIONS)
			{
				index = 0;
			}
			ssd1306_Fill(Black);
			if(event.name == BUTTON_DOWN && event.state == BUTTON_PRESS)
			{
				return;
			}
			if(event.name == BUTTON_UP)
			{
				switch(index)
				{
				case OPTION_DEBUG:
					debug_view();
					break;
				case OPTION_ONTIME:
					set_ontime_view();
					break;
				case OPTION_RESET_TRIP:
					reset_trip_view();
					break;
				case OPTION_SETWHEEL:
					set_wheel_view();
					break;
				case OPTION_DARKMODE:
					data.dark_theme = !data.dark_theme;
					ssd1306_DarkTheme(data.dark_theme);
					write_RTC(RTC_REGISTER_DARK_THEME, data.dark_theme);
				default:
					break;
				}
				ssd1306_Fill(Black);
			}
		}
		ssd1306_SetCursor(127/2 - 3*11/2, 0);
		ssd1306_WriteString("Menu", Font_11x18, White);
		sprintf(print_buffer,"%s",menu_option_text[index]);

		ssd1306_SetCursor(127/2 - (strlen(print_buffer)-1)*11/2, 63/2);

		ssd1306_WriteString(print_buffer, Font_11x18, White);

		draw_arrows(127/2,63/2);

		ssd1306_UpdateScreen();

		osDelay(10);
	}
}
void set_wheel_view()
{
	int32_t diameter;
	if(number_input(&diameter, "Diam (cm)", bike_parameters.diameter_cm, 100, 5, 5))
	{
		bike_parameters.diameter_cm = diameter;
		write_RTC(RTC_REGISTER_WHEEL_SIZE, diameter);
	}
}
void reset_trip_view()
{
	shared_data.current_pulses = 0;
	shared_data.current_time_s = 0;

	ssd1306_Fill(Black);
	ssd1306_SetCursor(127/2 - 3*11/2, 63/2);
	ssd1306_WriteString("Done", Font_11x18, White);
	ssd1306_UpdateScreen();

	while(1)
	{
		osDelay(50);
		struct button_event event;
		if(recv_button_event(&event))
		{
			if(event.state == BUTTON_PRESS)
			{
				return;
			}
		}
	}


}
uint8_t number_input(int32_t *number, char *title,int32_t start, int32_t upper, int32_t lower, uint8_t jump)
{
	int32_t value = start;
	ssd1306_Fill(Black);
	while(1)
	{
		osDelay(1);
		struct button_event event;
		if(recv_button_event(&event))
		{
			if(event.name == BUTTON_DOWN)
			{
				return 0;
			}
			if(event.name == BUTTON_UP)
			{
				*number = value;
				return 1;
			}
			if(event.name == BUTTON_LEFT)
			{
				if(event.state == BUTTON_PRESS)
				{
					value--;
				}
				else
				{
					value-=jump;
				}
			}
			if(event.name == BUTTON_RIGHT)
			{
				if(event.state == BUTTON_PRESS)
				{
					value++;
				}
				else
				{
					value+=jump;
				}

			}
			if(value < lower)
			{
				value = 0;
			}
			if(value > upper)
			{
				value = upper;
			}
			ssd1306_Fill(Black);
	}
	ssd1306_SetCursor(127/2 - (strlen(title)-1)*11/2, 0);
	ssd1306_WriteString(title, Font_11x18, White);
	sprintf(print_buffer,"%d",value);
	ssd1306_SetCursor(127/2 - (strlen(print_buffer)-1)*11/2, 63/2);

	ssd1306_WriteString(print_buffer, Font_11x18, White);
	draw_arrows(127/2,63/2);


	ssd1306_UpdateScreen();

	osDelay(50);
	}
}
void calc_ontime()
{
	uint32_t pulses = shared_data.current_pulses;

	float distance_m = 3.14 * (bike_parameters.diameter_cm/100.0) * pulses;

	uint32_t absoulute_time = shared_data.current_absolute_time_s;
	if(data.ontime_time_m*60-absoulute_time <= 0)
	{
		data.ontime_speed = 0;
		return;
	}
	float required_spd = (data.ontime_dist_km*1000-distance_m)/(data.ontime_time_m*60-absoulute_time);
	required_spd *=3.6;

	data.ontime_speed = required_spd;
	if(required_spd > 1000)
	{
		data.ontime = 0;
	}
	if(data.ontime_dist_km*1000-distance_m <= 0)
	{
		data.ontime = 0;
	}
}
void set_ontime_view()
{
	int32_t time = read_RTC(RTC_REGISTER_LAST_TIME);
	int32_t distance = read_RTC(RTC_REGISTER_LAST_DIST);
	if(time < 1)
	{
		time = 1;
	}
	if(distance < 1)
	{
		distance = 1;
	}
	data.ontime = 0;
	if(!number_input(&distance,"Dist (km)", distance, 1000, 1, 3))
	{
		return;
	}
	if(!number_input(&time,"Time (m)",time,1000,1,10))
	{
		return;
	}
	write_RTC(RTC_REGISTER_LAST_DIST, distance);
	write_RTC(RTC_REGISTER_LAST_TIME, time);
	data.ontime = 1;
	data.ontime_dist_km = distance;
	data.ontime_time_m = time;
	shared_data.current_absolute_time_s = 0;
	shared_data.current_pulses = 0;
	shared_data.current_time_s = 0;
	main_view();
	return;
}
void debug_view()
{
	while(1)
	{
		ssd1306_SetCursor(0, 0);
		int pulses = shared_data.current_pulses;
		int level = shared_data.adc_value;
		int speed_kph = shared_data.speed_kph;
		int time_diff = shared_data.time_diff_ms;
		int current_time = shared_data.current_time_s;


		sprintf(print_buffer,"Pulses %d",pulses);
		ssd1306_WriteString(print_buffer, Font_6x8, White);

		ssd1306_SetCursor(0, 30);

		sprintf(print_buffer,"Level %d",level);
		ssd1306_WriteString(print_buffer, Font_6x8, White);
		ssd1306_SetCursor(0, 50);


		sprintf(print_buffer,"Speed %4d",speed_kph);
		ssd1306_WriteString(print_buffer, Font_6x8, White);

		ssd1306_SetCursor(80, 30);
		RTC_TimeTypeDef time;
		HAL_RTC_GetTime(main_hrtc, &time, RTC_FORMAT_BIN);
		sprintf(print_buffer,"%2d:%2d:%2d",time.Hours,time.Minutes,time.Seconds);
		ssd1306_WriteString(print_buffer, Font_6x8, White);

		struct button_event event;

		uint32_t rtcval = read_RTC(10);

		ssd1306_SetCursor(80, 20);
		sprintf(print_buffer,"%d",rtcval);
		ssd1306_WriteString(print_buffer, Font_6x8, White);

		if(recv_button_event(&event))
		{
			int a = 0;
			a++;

			sprintf(print_buffer,"b%d",event.name);
			ssd1306_SetCursor(80, 0);

			write_RTC(10, rtcval+1);

			ssd1306_WriteString(print_buffer, Font_6x8, White);

		}
		ssd1306_SetCursor(70, 50);

		sprintf(print_buffer,"time %4d",time_diff);
		ssd1306_WriteString(print_buffer, Font_6x8, White);

		ssd1306_SetCursor(70, 40);

		sprintf(print_buffer,"t%4d",current_time);
		ssd1306_WriteString(print_buffer, Font_6x8, White);
		ssd1306_UpdateScreen();



		osDelay(100);
	}
}
void display_task(void* argument)
{
	//ssd1306_TestFPS();
	while(1)
	{
		main_view();
		menu_view();


	}
}
